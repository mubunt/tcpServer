//------------------------------------------------------------------------------
// Copyright (c) 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
public class tcpOut {
	private static final String OS = System.getProperty("os.name").toLowerCase();
	//---- COLOR ---------------------------------------------------------------
	private static final String ANSI_RESET	= "\u001B[0m";
	private static final String ANSI_RED 	= "\u001B[31m";
	private static final String ANSI_GREEN	= "\u001B[32m";
	private static final String ANSI_YELLOW	= "\u001B[33m";
	//--------------------------------------------------------------------------
	private static boolean isUnix() {
		return (OS.contains("nix") || OS.contains("nux"));
	}
	//--------------------------------------------------------------------------
	public static void error(String s) throws InterruptedException {
		if (isUnix())
			System.out.println(ANSI_RED + "ERROR: " + s + ANSI_RESET);
		else
			System.out.println("ERROR: " + s);
		Thread.sleep(10000);	//Pause for 10 seconds
		System.exit(-1);
	}
	//--------------------------------------------------------------------------
	public static void warning(String s) {
		if (isUnix())
			System.out.println(ANSI_YELLOW + "WARNING: " + s + ANSI_RESET);
		else
			System.out.println("WARNING: " + s);
	}
	//--------------------------------------------------------------------------
	public static void verbose(boolean verboseMode, String s) {
		if (verboseMode) {
			if (isUnix())
				System.out.println(ANSI_GREEN + "[VERBOSE] " + s + ANSI_RESET);
			else
				System.out.println("[VERBOSE] " + s);
		}
	}
	//--------------------------------------------------------------------------
	public static void version(String title, int version, int build, String date, int tcplibversion, int tcplibbuild, String tcplibdate) {
		System.out.println(title + " Version: " + version + " - Build: " + build + " - Date: " + date);
		System.out.println("	based on :");
		System.out.println("		tcpOut module Version: " + tcpOutBuildInfo.getVersion() + " Build: " + tcpOutBuildInfo.getNumber() + " - Date: " + tcpOutBuildInfo.getDate());
		System.out.println("		tcpLib module  Version: " + tcplibversion + " Build: " + tcplibbuild + " - Date: " + tcplibdate);
	}
	//--------------------------------------------------------------------------
}
