//------------------------------------------------------------------------------
// Copyright (c) 2016-2017, 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

class sensorUplet {	// A_Sensor_t - Size: 24 bytes
	int sensorId;		// 4 bytes
	long time;			// 8 bytes
	float value;		// 4 bytes
	long crc;			// 8 bytes
	//==== CODE ================================================================
	void unpack(byte[] bytes) {
		ByteBuffer bb = ByteBuffer.wrap(bytes);
		sensorId = bb.getInt();
		time = bb.getLong();
		value = bb.getFloat();
		crc = bb.getLong();
	}
	//--------------------------------------------------------------------------
	byte[] pack() {
		byte[] bytes = new byte[24];
		ByteBuffer buffer = ByteBuffer.wrap(bytes);
		buffer.order(ByteOrder.LITTLE_ENDIAN); // or BIG_ENDIAN
		buffer.putInt(0, sensorId);
		buffer.putLong(4, time);
		buffer.putFloat(12, value);
		buffer.putLong(16, crc);
		return buffer.array();
	}
	//--------------------------------------------------------------------------
	long computeCRC() {
		return (long) sensorId + time + (long) value;
	}
}
//==============================================================================
