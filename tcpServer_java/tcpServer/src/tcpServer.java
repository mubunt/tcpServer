//------------------------------------------------------------------------------
// Copyright (c) 2016-2017, 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.apache.commons.cli.*;

import java.util.concurrent.Semaphore;

class tcpServer {
	private static final int numberOfSensors = 16;
	private static final String sTitle = "Java Multithreaded Socket & Database Server";
	private static String dataBaseName = "tcpServer.sql";
	static boolean verboseMode = false;
	private static final Semaphore mutex = new Semaphore(1);
	//==== CODE ================================================================
	public static void main (String[] args) throws InterruptedException {
		tcpServerBuildInfo.logVersion(sTitle);
		// Command-line Options
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		options.addOption("v", "verbose",    false, "verbose mode");
		options.addOption("D", "database",   true,  "database file name");
		try {
			CommandLine line = parser.parse(options, args);
			if (line.hasOption("verbose")) verboseMode = true;
			if (line.hasOption("database")) dataBaseName = line.getOptionValue("database");
		} catch (ParseException exp) {
			tcpOut.error("Option parsing failed - " + exp.getMessage() + ". Quitting.");
		}
		//---- Database --------------------------------------------------------
		if (dataBaseName == null) {
			tcpOut.error("Database not specified. Quitting.");
		}
		dataBase connection = new dataBase(dataBaseName, numberOfSensors);
		connection.connect();
		//---- Go on using 2 threads -------------------------------------------
		tcpServerAcquisition acqThread = new tcpServerAcquisition();
		acqThread.start();
		tcpServerReporting repThread = new tcpServerReporting();
		repThread.start();
		while(acqThread.isAlive() || repThread.isAlive()) {
			try {
				Thread.sleep(800);
			}
			catch (InterruptedException e) {
				tcpOut.error("Abnormal situation: " + e.getMessage());
			}
		}
		//---- Database --------------------------------------------------------
		connection.close();
	}
	//--------------------------------------------------------------------------
	static void lockDatabaseAccess(){
		try {
			mutex.acquire();
		} catch (Exception e) {
			try { tcpOut.error("Cannot lock database. Quitting."); } catch (Exception ignore) { }
		}
	}
	static void unlockDatabaseAccess() {
		mutex.release();
	}
	//--------------------------------------------------------------------------
	//static byte[] htonl(int x) {
	//	byte[] res = new byte[4];
	//	for (int i = 0; i < 4; i++) {
	//		res[i] = (new Integer(x >>> 24)).byteValue();
	//		x <<= 8;
	//	}
	//	return res;
	//}
	//--------------------------------------------------------------------------
	static int ntohl(byte[] x) {
		int res = 0;
		for (int i = 0; i < 4; i++) {
			res <<= 8;
			res |= x[i];
		}
		return res;
	}
}
//==============================================================================
