//------------------------------------------------------------------------------
// Copyright (c) 2017, 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

class sensorTable {	// R_TableSent_t - Size: 28 bytes!!!!
	char name[];		// 12 bytes
	int size;			//  4 bytes
	int rows;			//  4 bytes
	long crc;			//  8 bytes
	//==== CODE ================================================================
	byte[] pack() {
		int i;
		byte[] bytes = new byte[28];
		ByteBuffer buffer = ByteBuffer.wrap(bytes);
		buffer.order(ByteOrder.LITTLE_ENDIAN); // or BIG_ENDIAN
		for (i = 0; i < name.length; i++) bytes[i] = (byte) name[i];
		for (; i < 12; i++) name[i] = 0;
		buffer.putInt(12, size);
		buffer.putInt(16, rows);
		buffer.putLong(20, crc);
		return buffer.array();
	}
	//--------------------------------------------------------------------------
	long computeCRC() {
		long crc = 0;
		for (int i = 0; i < 12; i++) crc = crc + (long) name[i];
		crc = crc + (long) size + rows;
		return crc;
	}
}
//==============================================================================
