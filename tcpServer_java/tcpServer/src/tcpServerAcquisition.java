//------------------------------------------------------------------------------
// Copyright (c) 2016-2017, 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
// Original design from:
//	http://www.mysamplecode.com/2011/12/java-multithreaded-socket-server.html
//------------------------------------------------------------------------------

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

class tcpServerAcquisition extends Thread {
	private static final int portAutomaton = 4321;
	private static ServerSocket	myServerSocket = null;
	//==== CODE ================================================================
	tcpServerAcquisition() {
		super();
	}
	//--------------------------------------------------------------------------
	public void run() {
		try {
			myServerSocket = new ServerSocket(portAutomaton);
		} catch(IOException e) {
			try {
				tcpOut.error("I/O Exception: " + e.getMessage());
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}
		// Successfully created Server Socket. Now wait for connections.
		tcpOut.verbose(tcpServer.verboseMode, "[Port " + portAutomaton + "] Waiting for connection...");
		//noinspection InfiniteLoopStatement
		while(true) {
			try {
				// Accept incoming connections.
				Socket clientSocket = myServerSocket.accept();
				// accept() will block until a client connects to the server.
				// For each client, we will start a service thread to service the client requests.
				// Starting a thread also lets our server accept multiple connections simultaneously.
				//tcpOut.verbose(tcpServer.verboseMode, "[Port " + portAutomaton + "] Accepted Client Address - " + clientSocket.getInetAddress().getHostName() + ".");
				ClientServiceThread cliThread = new ClientServiceThread(clientSocket);
				cliThread.start();
			}
			catch(IOException e) {
				tcpOut.warning("I/O Exception: " + e.getMessage() + ". Ignoring.");
			}
		}
	}
	//--------------------------------------------------------------------------
	static class ClientServiceThread extends Thread {
		Socket myClientSocket;
		private DataInputStream inFromClient = null;
		private boolean disconnected = false;
		//----------------------------------------------------------------------
		ClientServiceThread(Socket client) {
			myClientSocket = client;
		}
		//----------------------------------------------------------------------
		public void run() {
			try {
				inFromClient = new DataInputStream(myClientSocket.getInputStream());
			} catch (Exception e) {
				try { tcpOut.error("[ACQUISITION] Exception occurred when creating data stream: " + e.getMessage()); } catch (Exception ignore) { }
			}
			do {
				byte[] bytecommand = new byte[]{0, 0, 0, 0};
				assert inFromClient != null;
				try {
					//noinspection StatementWithEmptyBody
					while (0 == inFromClient.available()) ;
				} catch (Exception e) {
					try { tcpOut.error("[ACQUISITION] Exception occurred when waiting server: " + e.getMessage() + ". Quitting."); } catch (Exception ignore) { }
				}
				try {
					//noinspection StatementWithEmptyBody
					while (0 == inFromClient.read(bytecommand, 0, bytecommand.length)) ;
				} catch (Exception e) {
					try { tcpOut.error("[ACQUISITION] Exception occurred when reading command: " + e.getMessage() + ". Quitting."); } catch (Exception ignore) { }
				}
				try {
					commandProcessing(tcpServer.ntohl(bytecommand));
				} catch (Exception e) {
					try { tcpOut.error("[ACQUISITION] Exception occurred when executing command: " + e.getMessage() + ". Quitting."); } catch (Exception ignore) { }
				}
			} while (! disconnected);
			try {
				inFromClient.close();
				myClientSocket.close();
			} catch (Exception e) {
				try { tcpOut.error("[ACQUISITION] Exception occurred when closing socket:: " + e.getMessage() + ". Quitting."); } catch (Exception ignore) { }
			}
		}
		//----------------------------------------------------------------------
		private void commandProcessing(int command) throws Exception {
			switch (command) {
				case tcpLib.A_VALUES_SENT:
					sensorUplet	sensor = new sensorUplet();
					try {
						//noinspection StatementWithEmptyBody
						while (0 == inFromClient.available()) ;
					} catch (Exception e) {
						try { tcpOut.error("[ACQUISITION] Exception occurred when waiting server: " + e.getMessage()); } catch (Exception ignore) { }
					}
					byte[] bytevalue = new byte[24];
					try {
						inFromClient.readFully(bytevalue, 0, bytevalue.length);
					} catch (Exception e) {
						try { tcpOut.error("[ACQUISITION] Exception occurred when reading data: " + e.getMessage()); } catch (Exception ignore) { }
					}
					sensor.unpack(bytevalue);
					if (sensor.sensorId == -1) break;
					tcpOut.verbose(tcpServer.verboseMode, "[Port " + portAutomaton + "] Received Sensor " + String.format("%02d", sensor.sensorId)
							+ " (value=" + sensor.value + ", time=" + sensor.time + ", crc=" + sensor.crc + ")");
					long crc = sensor.computeCRC();
					if (crc != sensor.crc) {
						tcpOut.warning("[ACQUISITION] Bad CRC for request from sensor #" + sensor.sensorId + ": Received " + sensor.crc + " / Computed " + crc + ")");
					} else {
						try {
							tcpServer.lockDatabaseAccess();
							dataBase.addSensorUplet(sensor);
							tcpServer.unlockDatabaseAccess();
						} catch (Exception ignored) { }
					}
					break;
				case tcpLib.SERVER_DISCONNECT:
					disconnected = true;
					break;
				default:
					tcpOut.warning("[ACQUISITION] Unknown command (" + command + ") received from client.");
					disconnected = true;
					break;
			}
		}
	}
}
//==============================================================================
