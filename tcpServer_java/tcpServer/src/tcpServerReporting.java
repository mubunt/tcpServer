//------------------------------------------------------------------------------
// Copyright (c) 2016-2017, 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
// Original design from:
//	http://www.mysamplecode.com/2011/12/java-multithreaded-socket-server.html
//------------------------------------------------------------------------------
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.ResultSet;
import java.util.ArrayList;

class tcpServerReporting extends Thread {
	//==== DATA ================================================================
	private static final int portReporter = 6543;
	private static ServerSocket	myServerSocket = null;
	//==== CODE ================================================================
	tcpServerReporting() {
		super();
	}
	//--------------------------------------------------------------------------
	public void run() {
		try {
			myServerSocket = new ServerSocket(portReporter);
		} catch (IOException e) {
			try { tcpOut.error("I/O Exception: " + e.getMessage() + ". Quitting."); } catch (Exception ignore ) { }
		}
		// Successfully created Server Socket. Now wait for connections.
		tcpOut.verbose(tcpServer.verboseMode, "[Port " + portReporter + "] Waiting for connection...");
		//noinspection InfiniteLoopStatement
		while (true) {
			try {
				// Accept incoming connections.
				Socket clientSocket = myServerSocket.accept();
					// accept() will block until a client connects to the server.
					// For each client, we will start a service thread to service the client requests.
					// Starting a thread also lets our server accept multiple connections simultaneously.
				tcpOut.verbose(tcpServer.verboseMode, "[Port " + portReporter + "] Accepted Client Address - " + clientSocket.getInetAddress().getHostName() + ".");
				ClientServiceThread cliThread = new ClientServiceThread(clientSocket);
				cliThread.start();
			} catch(IOException e) {
				tcpOut.warning("I/O Exception: " + e.getMessage());
			}
		}
	}
	//--------------------------------------------------------------------------
	static class ClientServiceThread extends Thread {
		final Socket myClientSocket;
		private DataInputStream inFromClient = null;
		private DataOutputStream outToClient = null;
		private boolean disconnected = false;
		//----------------------------------------------------------------------
		ClientServiceThread(Socket s) {
			myClientSocket = s;
		}
		//----------------------------------------------------------------------
		public void run() {
			// Input stream to receive values from the client
			try {
				inFromClient = new DataInputStream(myClientSocket.getInputStream());
				outToClient = new DataOutputStream(myClientSocket.getOutputStream());
			} catch (Exception e) {
				try { tcpOut.error("[REPORTING] Exception occurred when creating data streams: " + e.getMessage()); } catch (Exception ignore) { }
			}
			do {
				byte[] bytecommand = new byte[] {-1, 0, 0, 0};
				assert inFromClient != null;
				try {
					//noinspection StatementWithEmptyBody
					while (0 == inFromClient.available());
				} catch (Exception e) {
					try { tcpOut.error("[REPORTING] Exception occurred when waiting server - " + e.getMessage() + ". Quitting."); } catch (Exception ignore) { }
				}
				try {
					//noinspection StatementWithEmptyBody
					while (0 == inFromClient.read(bytecommand, 0, bytecommand.length));
				} catch (Exception e) {
					try { tcpOut.error("[REPORTING] Exception occurred when reading command: " + e.getMessage() + ". Quitting."); } catch (Exception ignore) { }
				}
				try {
					commandProcessing(tcpServer.ntohl(bytecommand));
				} catch (Exception e) {
					try { tcpOut.error("[REPORTING] Exception occurred when executing command: " + e.getMessage() + ". Quitting."); } catch (Exception ignore) { }
				}
			} while (! disconnected);
			try {	
				inFromClient.close();
				outToClient.close();
				myClientSocket.close();
			} catch (Exception e) {
				try { tcpOut.error("Exception occurred when closing socket:: " + e.getMessage() + ". Quitting."); } catch (Exception ignore) { }
			}
		}
		//----------------------------------------------------------------------
		private void commandProcessing(int command) throws Exception {
			switch (command) {
				case tcpLib.R_VALUES_REQUESTED:
					byte[] bytevalue1 = new byte[28];
					// Read the request (sensorRequest)
					sensorRequest sensor = new sensorRequest();
					try {
						inFromClient.readFully(bytevalue1, 0, bytevalue1.length);
					} catch (Exception e) {
						tcpOut.error("[REPORTING] Exception occurred when reading data: " +
								e.getMessage() + ". Quitting.");
					}
					sensor.unpack(bytevalue1);
					sensorUplet foundValue = new sensorUplet();
					// Check CRC
					long crc = sensor.computeCRC();
					if (crc != sensor.crc) {
						tcpOut.warning("[REPORTING] Bad CRC for request from sensor #" + sensor.sensorId + ": Received " + sensor.crc + " / Computed " + crc + ")");
					} else {
						// Processing the request
						tcpServer.lockDatabaseAccess();
						ResultSet resultSet = dataBase.getValues(sensor);
						// Send the results  (sensorUplet) to the client
						if (resultSet != null) {
							while (resultSet.next()) {
								try {
									foundValue.sensorId = resultSet.getInt("IDX");
									foundValue.time = resultSet.getLong("TIME");
									foundValue.value = resultSet.getFloat("VALUE");
									foundValue.crc = foundValue.computeCRC();
								} catch (Exception e) {
									tcpOut.warning("[REPORTING] Exception occurred when retrieving data (" +
											e.getMessage() + "). Abandonment of the transaction.");
									break;
								}
								bytevalue1 = foundValue.pack();
								try {
									assert outToClient != null;
									outToClient.write(bytevalue1, 0, bytevalue1.length);
								} catch (Exception e) {
									tcpOut.error("[REPORTING] Exception occurred when writing data to client: " +
											e.getMessage() + ". Quitting.");
								}
							}
						}
						tcpServer.unlockDatabaseAccess();
					}
					// Send end of data
					foundValue.sensorId = -1;
					foundValue.time = 0;
					foundValue.value = (float) 0;
					foundValue.crc = foundValue.computeCRC();
					bytevalue1 = foundValue.pack();
					try {
						assert outToClient != null;
						outToClient.write(bytevalue1, 0, bytevalue1.length);
						outToClient.flush();
					} catch (Exception e) {
						tcpOut.error("[REPORTING] Exception occurred when writing end of data to client: " +
								e.getMessage() + ". Quitting.");
					}
					break;
				case tcpLib.R_TABLES_REQUESTED:
					// Database query
					ArrayList<String> tableList;
					tcpServer.lockDatabaseAccess();
					tableList = dataBase.getTable();
					tcpServer.unlockDatabaseAccess();
					// Allocation
					byte[] bytevalue2;
					sensorTable table = new sensorTable();
					table.name = new char[12];
					// Sending
					for (String str : tableList) {
						int i;
						for (i = 0; i < str.length(); i++) table.name[i] = str.toCharArray()[i];
						for (; i < 12; i++) table.name[i] = 0;
						table.size = str.length();
						tcpServer.lockDatabaseAccess();
						table.rows = dataBase.getCount(str);
						tcpServer.unlockDatabaseAccess();
						table.crc = table.computeCRC();
						bytevalue2 = table.pack();
						try {
							assert outToClient != null;
							outToClient.write(bytevalue2, 0, bytevalue2.length);
						} catch (Exception e) {
							tcpOut.error("[REPORTING] Exception occurred when writing data to client: " +
									e.getMessage() + ". Quitting.");
						}
					}
					// End of sending
					for (int i = 0; i < 12; i++) table.name[i] = 0;
					table.size = table.rows = 0;
					table.crc = table.computeCRC();
					bytevalue2 = table.pack();
					try {
						assert outToClient != null;
						outToClient.write(bytevalue2, 0, bytevalue2.length);
						outToClient.flush();
					} catch (Exception e) {
						tcpOut.error("[REPORTING] Exception occurred when writing end of data to client: " +
								e.getMessage() + ". Quitting.");
					}
					break;
				case tcpLib.SERVER_DISCONNECT:
					disconnected = true;
					break;
				default:
					tcpOut.warning("[REPORTING] Unknown command (" + command + ") received from client.");
					disconnected = true;
					break;
			}
		}
	}
}
//==============================================================================
