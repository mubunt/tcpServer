//------------------------------------------------------------------------------
// Copyright (c) 2016-2017, 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import java.io.File;
import java.sql.*;
import java.util.ArrayList;

class dataBase {
	private static String databasePathName = null;
	private static int sensorNumber;
	private static Connection connection = null;
	private static Statement statement = null;

	private static final String sqlCREATE =
		"CREATE TABLE SENSOR%03d(IDX INT PRIMARY KEY NOT NULL, TIME INT NOT NULL, VALUE REAL NOT NULL) WITHOUT ROWID; ";
	//==== CODE ================================================================
	dataBase(String dBPath, @SuppressWarnings("SameParameterValue") int sensors) {
		databasePathName = dBPath;
		sensorNumber = sensors;
	}
	//--------------------------------------------------------------------------
	void connect() throws InterruptedException {
		File f = new File(databasePathName);
		if (f.exists())
			try {
				Class.forName("org.sqlite.JDBC");
				connection = DriverManager.getConnection("jdbc:sqlite:" + databasePathName);
				statement = connection.createStatement();
				tcpOut.verbose(tcpServer.verboseMode, "Successful connection to " + databasePathName + ".");
			} catch (ClassNotFoundException e) {
				tcpOut.error("[DATABASE] Connection error: " + e.getMessage());
			} catch (SQLException e) {
				tcpOut.error("[DATABASE] Connection error ('create statement' SQL exception): " + e.getMessage());
			}
		else {
			try {
				Class.forName("org.sqlite.JDBC");
				connection = DriverManager.getConnection("jdbc:sqlite:" + databasePathName);
				statement = connection.createStatement();
			} catch (ClassNotFoundException e) {
				tcpOut.error("[DATABASE] Connection error (class not found): " + e.getMessage());
			} catch (SQLException e) {
				tcpOut.error("[DATABASE] Connection error ('create statement' SQL exception): " + e.getMessage());
			}
			try {
				String cmd = "";
				for (int i = 0; i < sensorNumber; ++i) {
					cmd += String.format(sqlCREATE, i);
				}
				statement.executeUpdate(cmd);
				tcpOut.verbose(tcpServer.verboseMode, "Successful creation of " + databasePathName + ".");
			} catch (SQLException e) {
				tcpOut.error("[DATABASE] Database creation error ('create' SQL exception): " + e.getMessage());
			}
		}
	}
	//--------------------------------------------------------------------------
	void close() throws InterruptedException {
		try {
			connection.close();
			statement.close();
		} catch (SQLException e) {
			tcpOut.error("[DATABASE] Cannot close database.");
		}
	}
	//--------------------------------------------------------------------------
	static void addSensorUplet(sensorUplet values) throws SQLException {
		String tableName = "SENSOR" + String.format("%03d", values.sensorId);
		int count = getCount(tableName);
		String query = "INSERT INTO " + tableName + "(IDX, TIME, VALUE) VALUES (" +
				count + ", " + values.time + ", " + values.value + ");";
		//tcpOut.verbose(tcpServer.verboseMode, "[ADDUPLET] Query: " + query);
		statement.executeUpdate(query);
	}
	//--------------------------------------------------------------------------
	static ResultSet getValues(sensorRequest values) throws SQLException {
		String query;
		String tableName = "SENSOR" + String.format("%03d", values.sensorId);
		if (values.timestart == -1 && values.timeend == -1) {
			query = "SELECT IDX, TIME, VALUE FROM " + tableName + ";";
		} else {
			if (values.timestart == -1) {
				query = "SELECT IDX, TIME, VALUE FROM " + tableName + " WHERE TIME <= " + values.timeend + ";";
			} else {
				if (values.timeend == -1) {
					query = "SELECT IDX, TIME, VALUE FROM " + tableName + " WHERE TIME >= " + values.timestart + ";";
				} else {
					query = "SELECT IDX, TIME, VALUE FROM " + tableName + 
							" WHERE TIME >= " + values.timestart + " AND TIME <= " + values.timeend + ";";
				}
			}
		}
		//tcpOut.verbose(tcpServer.verboseMode, "[GETVALUES] Query: " + query);
		return statement.executeQuery(query);
	}
	//--------------------------------------------------------------------------
	static int getCount(String tableName) throws SQLException {
		int count = 0;
		String query = "SELECT COUNT(*) FROM " + tableName;
		//tcpOut.verbose(tcpServer.verboseMode, "[GETCOUNT] Query: " + query);
		ResultSet rs = statement.executeQuery(query);
		if (rs != null) {
			rs.next();
			count = rs.getInt(1);
		}
		return count;
	}
	//--------------------------------------------------------------------------
	static ArrayList<String> getTable() throws SQLException {
		ArrayList<String> tableList = new ArrayList<>();
		DatabaseMetaData md = connection.getMetaData();
		ResultSet rs = md.getTables(null, null, "%", null);
		if (rs != null) {
			while (rs.next()) {
				tableList.add(rs.getString(3));
			}
		}
		return tableList;
	}
}
//==============================================================================
