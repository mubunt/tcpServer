#------------------------------------------------------------------------------
# Copyright (c) 2017, 2019, Michel RIZZO.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Multithreaded Socket & Database Server
#-------------------------------------------------------------------------------
PROGRAM			= clientAutomaton
BINJAR			= ../tcpServer_bin
IDEA_PRJ		= $(PROGRAM)
COMMONSCLI		= $(notdir $(shell ls $(BINJAR)/commons-cli-*))
TCPLIB			= $(notdir $(shell ls $(BINJAR)/tcpLib*))
TCPOUT			= $(notdir $(shell ls $(BINJAR)/tcpOut*))
JARS			= $(COMMONSCLI)$(TCPLIB) $(TCPOUT)
VERSINFO		= $(IDEA_PRJ)/src/clientBuildInfo.java
MOD_VERSINFO	= $(IDEA_PRJ)/src/modified_clientBuildInfo.java
SOURCES			= $(IDEA_PRJ)/src/clientAutomaton.java

include ../.make/java.mk
#-------------------------------------------------------------------------------
