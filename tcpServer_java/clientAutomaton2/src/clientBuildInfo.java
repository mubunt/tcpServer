//------------------------------------------------------------------------------
// Copyright (c) 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
class clientBuildInfo {
	@SuppressWarnings("SameReturnValue")
	static String getVersion() { return "@UNKNOWN@"; }
	@SuppressWarnings("SameReturnValue")
	private static int getNumber() { return 2; }
	@SuppressWarnings("SameReturnValue")
	private static String getDate() { return "November, 2019"; }
	static void logVersion(String title) {
		System.out.println(title + 
            " Version: " + getVersion() +
            " - Build: " + getNumber() + 
            " - Date: " + getDate());
	}
}
