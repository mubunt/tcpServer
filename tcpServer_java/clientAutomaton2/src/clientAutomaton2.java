//------------------------------------------------------------------------------
// Copyright (c) 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import org.apache.commons.cli.*;

import javax.swing.*;
import java.awt.*;

class clientAcquisition extends Thread {
	private static final int portID = 4321;
	private int threadNumber;
	private String serverID;
	private boolean verboseMode;
	private static ThreadLocal<Integer> myvalue = ThreadLocal.withInitial (() -> 0);

	clientAcquisition(int threadNumber, String serverID, boolean verboseMode) {
		this.threadNumber = threadNumber;
		this.serverID = serverID;
		this.verboseMode = verboseMode;
	}
	public void run() {
		tcpOut.verbose(verboseMode, "Sensor " + String.format("%02d", threadNumber) + " started...");
		sensorUplet acq = new sensorUplet();
		while (true) {
			acq.sensorId = threadNumber;
			acq.value = getValue();
			acq.time = System.currentTimeMillis();
			acq.crc = acq.computeCRC();
			byte[] bytevalue = acq.pack();
			try {
				boolean b = true;
				clientAutomaton2.counters[threadNumber]++;
				do {
					tcpOut.verbose(verboseMode, "Sensor " + String.format("%02d", threadNumber)
							+ (b ? " sending ":" resending ") + acq.value + " at " + acq.time
							+ ". Total: " + clientAutomaton2.counters[threadNumber]);
					int tries = 0;
					do {
						if (tries > 0)
							tcpOut.warning("Connection failed. Retry #" + tries);
						b = tcpLib.connectServer (serverID, portID);
						++tries;
					} while (!b && tries < 4);
					if (!b) {
						tcpOut.error("Cannot connect to server.");
					}
					b = tcpLib.sendValue2Server(bytevalue, bytevalue.length);
					tcpLib.disconnectServer();
					clientAutomaton2.displayCounter(threadNumber);
				} while (!b);
				Thread.sleep(2000 + threadNumber * 500);
				//Thread.sleep(2000);
			} catch (Exception ignored) { }
		}
	}
	private float getValue() {
		int value = myvalue.get();
		if (value == 10) {
			value = 0;
			myvalue.set(value);
		}
		value += (threadNumber * 10);
		plusplus();
		return (float) value;
	}
	private void plusplus() {
		int value1 = myvalue.get();
		++value1;
		myvalue.set(value1);
	}
}

class clientAutomaton2 {
	private static final int numberSensors = 16;
	private static final int NUMBERofCOLS = 4;
	private static final String	sTitle = "Simple Java Acquisition Client";
	private static boolean verboseMode = false;
	static int[] counters = new int[numberSensors];
	private static final JLabel[] countLabels = new JLabel[numberSensors];
	private static final JLabel[] titleLabels = new JLabel[numberSensors];
	//---- SERVER --------------------------------------------------------------
	private static String serverID = null;
	//==========================================================================
	public static void main(String[] args) throws InterruptedException {
		clientBuildInfo.logVersion(sTitle);
		Runtime.getRuntime().addShutdownHook(new Thread(() -> System.out.println("\nInterrupt catched. Stopping...")));
		// OPTIONS
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		options.addOption("v", "verbose",	false, "verbose mode");
		options.addOption("S", "server",	true,  "server IP address");
		try {
			CommandLine line = parser.parse(options, args);
			if (line.hasOption("verbose")) verboseMode = true;
			if (line.hasOption("server")) serverID = line.getOptionValue("S");
		} catch (ParseException exp) {
			tcpOut.error("Option parsing failed - " + exp.getMessage() + ". Quitting.");
		}
		if (serverID == null) serverID = "localhost";

		displayCounters();

		clientAcquisition[] clientacq = new clientAcquisition[numberSensors];
		for (int i = 0; i < numberSensors; i++ ) {
			clientacq[i] = new clientAcquisition(i, serverID, verboseMode);
			clientacq[i].start();
		}
		boolean b = true;
		while (b) {
			try {
				Thread.sleep(800);
			}
			catch (InterruptedException e) {
				tcpOut.error("Abnormal situation: " + e.getMessage());
			}
			for (int i = 0; i < numberSensors; i++ ) b &= clientacq[i].isAlive();
		}
		System.exit(0);
	}
	//--------------------------------------------------------------------------
	static void displayCounter(int sensor) {
		countLabels[sensor].setText(String.format("%05d", counters[sensor]));
	}
	//--------------------------------------------------------------------------
	private static void displayCounters() {
		//---- Font for sensor names -------------------------------------------
		Font counterFont = new Font("Arial", Font.BOLD, 20);
		Font titleFont = new Font("Arial", Font.ITALIC, 10);
		//---- Frame -----------------------------------------------------------
		JFrame mainFrame = new JFrame();
		mainFrame.setTitle("Acquisition" +
				" - Number of values sent by sensor");
		mainFrame.setSize(400, 200);
		mainFrame.setLocation(0, 0);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setResizable(false);
		//---- Panel -----------------------------------------------------------
		JPanel panel = new JPanel();
		panel.setLayout(null);
		mainFrame.setContentPane(panel);
		//---- Counters and Labels ---------------------------------------------
		for (int i=0; i< countLabels.length; i++) {
			counters[i] = 0;
			titleLabels[i] = new JLabel(String.format("Sensor%02d",i));
			titleLabels[i].setFont(titleFont);
			titleLabels[i].setPreferredSize(new Dimension(60, 15));
			titleLabels[i].setBounds(new Rectangle(new Point(getX(i),getY(i)), titleLabels[i].getPreferredSize()));
			countLabels[i] = new JLabel(String.format("%05d", counters[i]));
			countLabels[i].setFont(counterFont);
			countLabels[i].setPreferredSize(new Dimension(80, 25));
			countLabels[i].setBounds(new Rectangle(new Point(getX(i),getY(i) + 10), countLabels[i].getPreferredSize()));
			countLabels[i].setBackground(Color.WHITE);
			//countLabels[i].setOpaque(true);
			//countLabels[i].setBackground(Color.WHITE);
			mainFrame.add(titleLabels[i]);
			mainFrame.add(countLabels[i]);
		}
		mainFrame.setVisible(true);
	}
	//--------------------------------------------------------------------------
	private static int getX(int id) {
		return((id % NUMBERofCOLS) * 90 + 20);
	}
	//--------------------------------------------------------------------------
	private static int getY(int id) {
		return((id / NUMBERofCOLS) * 40 + 10);
	}
}
