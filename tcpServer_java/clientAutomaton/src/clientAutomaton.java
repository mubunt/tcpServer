//------------------------------------------------------------------------------
// Copyright (c) 2016-2017, 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.apache.commons.cli.*;

import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.TimeZone;

class clientAutomaton {
	private static final int numberSensors = 16;
	private static final String	sTitle = "Simple Java Acquisition Client";
	private static boolean verboseMode = false;
	private static final int[] Record = new int[numberSensors];
	//---- SERVER --------------------------------------------------------------
	private static final int portID = 4321;
	private static String serverID = null;
	//---- VALUE COMPUTING -----------------------------------------------------
	private static double value = 0;
	//==========================================================================
	public static void main(String[] args) throws Exception {
		clientBuildInfo.logVersion(sTitle);
		for (int i = 0; i < numberSensors; i++)
			Record[i] = -1;
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("\nInterrupt catched. Stopping...");
            System.out.println("\nSUMMARY\n");
            System.out.print("--------------------------------------------------------------------------");
            for (int sensor = 0; sensor < numberSensors; sensor ++) {
                if (0 == sensor % 4) System.out.print("\n| ");
                System.out.print(String.format("Sensor %02d: %05d |", sensor, Record[sensor] + 1));
            }
            System.out.println("\n--------------------------------------------------------------------------");
        }));

		// OPTIONS
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		options.addOption("v", "verbose",	false, "verbose mode");
		options.addOption("S", "server",	true,  "server IP address");
		try {
			CommandLine line = parser.parse(options, args);
			if (line.hasOption("verbose")) verboseMode = true;
			if (line.hasOption("server")) serverID = line.getOptionValue("S");
		} catch (ParseException exp) {
			tcpOut.error("Option parsing failed - " + exp.getMessage() + ". Quitting.");
		}
		if (serverID == null) serverID = "localhost";

		//noinspection InfiniteLoopStatement
		while (true) {
			byte[] bytevalue = randValue();
			boolean b = tcpLib.connectServer(serverID, portID);
			b = tcpLib.sendValue2Server(bytevalue, bytevalue.length);
			tcpLib.disconnectServer();
			Thread.sleep(500);	//Pause for 1/2 second
		}
	}
	//--------------------------------------------------------------------------
	private static byte[] randValue() {
		sensorUplet acq = new sensorUplet();
		acq.sensorId = randInt(numberSensors - 1);
		acq.value = getValue();
		acq.time = System.currentTimeMillis();
		acq.crc = acq.computeCRC();
		Record[acq.sensorId]++;
		tcpOut.verbose(verboseMode, "Sensor " + String.format("%02d", acq.sensorId) +
				" : Record " + Record[acq.sensorId] +
				" - value=" + acq.value + ", time=" + convert(acq.time) + " (" + acq.time + ")");
		return acq.pack();
	}
	//--------------------------------------------------------------------------
	private static int randInt(int max) {
		Random rand = new Random();
		return rand.nextInt((max) + 1);
	}
	//--------------------------------------------------------------------------
	private static float getValue() {
		value += .1;
		if (value > 2 * Math.PI) value = 0;
		int intValue = (int) ((Math.sin(value) + (Math.sin(3 * value) / 3)) * 100);
		return (float)intValue;
	}
	//--------------------------------------------------------------------------
	private static String convert(long timer) {
		SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:MM:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		return formatter.format(timer);
	}
}
