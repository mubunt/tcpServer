//------------------------------------------------------------------------------
// Copyright (c) 2017, 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.apache.commons.cli.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Random;
import java.util.TimeZone;

class clientReporter {
	private static final int numberSensors = 16;
	private static final String sTitle = "Simple Java Reporting Client";
	//---- SERVER --------------------------------------------------------------
	private static final int portID = 6543;
	private static String serverID = null;
	//==========================================================================
	public static void main(String[] args) throws Exception {
		clientBuildInfo.logVersion(sTitle);
		// OPTIONS
		boolean verboseMode = false;
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		options.addOption("S", "server",	true,  "server IP address");
		options.addOption("v", "verbose",    false, "verbose mode");
		try {
			CommandLine line = parser.parse(options, args);
			if (line.hasOption("server")) serverID = line.getOptionValue("S");
			if (line.hasOption("verbose")) verboseMode = true;
		} catch (ParseException e) {
			tcpOut.error("Option parsing failed - " + e.getMessage() + ". Quitting.");
		}
		if (serverID == null) serverID = "localhost";

		// TEST #1
		tcpLib.connectServer(serverID, portID);
		ArrayList<sensorIF.sensorTable> sensorTable = tcpLib.getSensorTable();
		tcpLib.disconnectServer();
		displaySensorTable(sensorTable);

		// TEST #2
		//noinspection InfiniteLoopStatement
		while (true) {
			boolean b = tcpLib.connectServer(serverID, portID);
			int sensorId = randInt(numberSensors - 1);
			ArrayList<sensorIF.sensorData> sensorData = tcpLib.getSensorData(sensorId);
			tcpLib.disconnectServer();
			displaySensorData(sensorId, sensorData);
			Thread.sleep(1000); 	//Pause for 1 second
		}
	}
	//--------------------------------------------------------------------------
	private static void displaySensorData(int id, ArrayList<sensorIF.sensorData> data) {
		System.out.println("--------------------------------------------------------------------");
		System.out.println("| Sensor = " + id);
		System.out.println("--------------------------------------------------------------------");
		data.forEach(sensor -> System.out.println("| Rank=" + sensor.sensorId + " - time=" + convert(sensor.time)
				+ " (" + sensor.time + ") - value=" + sensor.value));
		System.out.println("--------------------------------------------------------------------");
	}

	private static void displaySensorTable(ArrayList<sensorIF.sensorTable> table) {
		System.out.println("----------------------------");
		System.out.println("| Tables and Rows");
		System.out.println("----------------------------");
		table.forEach(sensor -> System.out.println("| " + new String(sensor.name) + " | " + sensor.rows));
		System.out.println("----------------------------");
	}
	//--------------------------------------------------------------------------
	private static int randInt(@SuppressWarnings("SameParameterValue") int max) {
		Random rand = new Random();
		return rand.nextInt((max) + 1);
	}
	//--------------------------------------------------------------------------
	private static String convert(long timer) {
		SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:MM:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		return formatter.format(timer);
	}
}
