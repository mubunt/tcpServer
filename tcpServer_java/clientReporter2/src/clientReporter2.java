//------------------------------------------------------------------------------
// Copyright (c) 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import org.apache.commons.cli.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

public class clientReporter2 {
	private static final int numberSensors = 16;
	private static final String sTitle = "Simple Java Reporting Client";
	private static boolean verboseMode = false;
	private static final int reminderTime = 10;
	//---- SERVER --------------------------------------------------------------
	private static final int portID = 6543;
	private static String serverID = null;
	//---- DISPLAY -------------------------------------------------------------
	private static Display display;
	private static final int BASEx = 500;
	private static final int Width = 1400;
	private static final int Height = 800;
	private static Text[] TextTable;
	//==========================================================================
	public static class ReminderList {
		Timer timer;
		ReminderList(int seconds) {
			timer = new Timer();
			timer.schedule(new FillContent(), 0,seconds * 1000);
		}
		static class FillContent extends TimerTask {
			public void run() {
				tcpOut.verbose(verboseMode, "Time's up!");
				try {
					boolean b;
					int tries = 0;
					do {
						if (tries > 0) {
							tcpOut.warning("Connection failed. Retry #" + tries);
							Thread.sleep(2000);
						}
						b = tcpLib.connectServer(serverID, portID);
						++tries;
					} while (!b && tries < 4);
					if (!b) {
						tcpOut.error("Cannot connect to server.");
					}
				} catch (Exception ignored) { }
				for (int sensorId = 0; sensorId < numberSensors; sensorId++) {
					ArrayList<sensorIF.sensorData> sensorData = null;
					try {
						sensorData = tcpLib.getSensorData(sensorId);
					} catch (Exception ignored) { }
					assert sensorData != null;
					if (sensorData.size() != 0) {
						for (sensorIF.sensorData sensorDatum : sensorData) {
							int finalSensorId = sensorId;
							display.asyncExec(() -> TextTable[finalSensorId].append("Sensor=" + finalSensorId + " - Rank=" + sensorDatum.sensorId + " - time=" + convert(sensorDatum.time) + " - value=" + sensorDatum.value + "\n"));
						}
						int finalSensorId = sensorId;
						display.asyncExec(() -> TextTable[finalSensorId].append("----------------------------------------------------------------------\n"));
					}
				}
				try {
					tcpLib.disconnectServer();
				} catch (Exception ignored) { }
			}
		}
	}
	//==========================================================================
	public static void main(String[] args) throws InterruptedException {
		clientBuildInfo.logVersion(sTitle);
		// OPTIONS
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		options.addOption("S", "server",	true,  "server IP address");
		options.addOption("v", "verbose", false, "verbose mode");
		try {
			CommandLine line = parser.parse(options, args);
			if (line.hasOption("server")) serverID = line.getOptionValue("S");
			if (line.hasOption("verbose")) verboseMode = true;
		} catch (ParseException e) {
			tcpOut.error("Option parsing failed - " + e.getMessage() + ". Quitting.");
		}
		if (serverID == null) serverID = "localhost";
		// UI
		display = new Display();
		Shell shell = new Shell(display, SWT.MAX | SWT.MIN | SWT.CLOSE | SWT.TITLE | SWT.BORDER | SWT.RESIZE);
		shell.setText(sTitle);
		shell.setLayout(new GridLayout(4, true));
		shell.setBounds(BASEx, 0, Width, Height);
		TextTable = new Text[numberSensors];
		for (int i = 0; i < numberSensors; i++) {
			TextTable[i] = new Text(shell, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.WRAP);
			GridData gridData = new GridData();
			gridData.horizontalAlignment = GridData.FILL;
			gridData.verticalAlignment = GridData.FILL;
			gridData.grabExcessHorizontalSpace = true;
			gridData.grabExcessVerticalSpace = true;
			TextTable[i] .setLayoutData(gridData);
		}
		shell.open();
		// TIMER
		tcpOut.verbose(verboseMode, "About to schedule task (every " + reminderTime + " seconds.");
		new ReminderList(reminderTime);
		tcpOut.verbose(verboseMode, "Task scheduled.");
		//
		while (!shell.isDisposed())
			if (!display.readAndDispatch())
				display.sleep();
		display.dispose();
	}
	//--------------------------------------------------------------------------
	private static String convert(long timer) {
		//SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:MM:ss");
		SimpleDateFormat formatter = new SimpleDateFormat("HH:MM:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		return formatter.format(timer);
	}
}
