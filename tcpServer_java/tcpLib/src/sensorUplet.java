//------------------------------------------------------------------------------
// Copyright (c) 2016-2017, 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class sensorUplet { // A_Sensor_t - Size: 24 bytes
	public int sensorId;
	public long	time;
	public float value;
	public long	crc;
	//==========================================================================
	public static byte[] packInt(int value) {
		byte[] ba = new byte[4];
		ByteBuffer bb = ByteBuffer.wrap(ba);
		bb.order(ByteOrder.BIG_ENDIAN); // or LITTLE_ENDIAN
		bb.putInt(value);
		return ba;
	}
	//----------------------------------------------------------------------
	public byte[] pack() {
		byte[] bytes = new byte[24];
		ByteBuffer buffer = ByteBuffer.wrap(bytes);
		buffer.order(ByteOrder.BIG_ENDIAN); // or LITTLE_ENDIAN
		buffer.putInt(0, sensorId);
		buffer.putLong(4, time);
		buffer.putFloat(12, value);
		buffer.putLong(16, crc);
		return buffer.array();
	}
	//----------------------------------------------------------------------
	public long computeCRC() {
		return (long) sensorId + time + (long) value;
	}
}
