//------------------------------------------------------------------------------
// Copyright (c) 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class tcpLib {
	private static Socket clientSocket = null;
	private static DataOutputStream outToServer = null;
	private static DataInputStream inFromServer = null;
	//--------------------------------------------------------------------------
	public static final int R_VALUES_REQUESTED	= 1;
	public static final int R_TABLES_REQUESTED	= 2;
	public static final int A_VALUES_SENT = 16;
	public static final int SERVER_DISCONNECT	= 64;
	//--------------------------------------------------------------------------
	public static boolean connectServer(String server, int portID) throws Exception {
		try {
			clientSocket = new Socket(server, portID);
		} catch (Exception e) {
			tcpOut.warning("Exception occurred on connect - " + e.getMessage());
			return false;
		}
		try {
			outToServer = new DataOutputStream(clientSocket.getOutputStream());
		} catch (Exception e) {
			tcpOut.warning("Exception occurred on connect - " + e.getMessage());
			clientSocket.close();
			return false;
		}
		try {
			inFromServer = new DataInputStream(clientSocket.getInputStream());
		} catch (Exception e) {
			tcpOut.warning("Exception occurred on connect - " + e.getMessage());
			outToServer.close();
			clientSocket.close();
			return false;
		}
		return true;
	}
	//--------------------------------------------------------------------------
	public static void disconnectServer() {
		byte[] bacli = sensorIF.packInt(SERVER_DISCONNECT);
		try {
			outToServer.write(bacli, 0, bacli.length);
			outToServer.flush();
			inFromServer.close();
			outToServer.close();
			clientSocket.close();
		} catch (Exception e) {
			tcpOut.warning("Exception occurred on disconnect - " + e.getMessage());
		}
	}
	//--------------------------------------------------------------------------
	public static boolean sendValue2Server(byte[] bytevalue, int len) throws Exception {
		byte[] bacli = sensorUplet.packInt(A_VALUES_SENT);
		try {
			//outToServer.write(bacli, 0, bacli.length);
			outToServer.write(bacli, 0, 4);
			outToServer.write(bytevalue, 0, len);
			outToServer.flush();
		} catch (Exception e) {
			tcpOut.warning("Exception occurred when sending data - " + e.getMessage());
			return false;
		}
		return true;
	}
	//--------------------------------------------------------------------------
	public static ArrayList<sensorIF.sensorData> getSensorData(int id) throws Exception {
		sensorIF.sensorRequest requested_sensor = new sensorIF.sensorRequest();
		byte[] bacli = sensorIF.packInt(R_VALUES_REQUESTED);
		requested_sensor.sensorId = id;
		requested_sensor.timestart = -1;
		requested_sensor.timeend = -1;
		requested_sensor.crc = requested_sensor.computeCRC();
		byte[] basrv2 = requested_sensor.pack();
		try {
			outToServer.write(bacli, 0, bacli.length);
			outToServer.write(basrv2, 0, basrv2.length);
			outToServer.flush();
		} catch (Exception e) {
			tcpOut.error("Exception occurred when getting data - " + e.getMessage());
		}
		try {
			//noinspection StatementWithEmptyBody
			while (0 == inFromServer.available()) ;
		} catch (Exception e) {
			tcpOut.error("Exception occurred when waiting server - " + e.getMessage());
		}
		ArrayList<sensorIF.sensorData> data = new ArrayList<>();
		while (true) {
			sensorIF.sensorData sensor = new sensorIF.sensorData();
			byte[] basrv3 = new byte[24];
			try {
				inFromServer.readFully(basrv3, 0, basrv3.length);
			} catch (Exception e) {
				tcpOut.error("Exception occurred when reading data - " + e.getMessage());
			}
			sensor.unpack(basrv3);
			if (sensor.sensorId == -1) break;
			long crc = sensor.computeCRC();
			// Workaround for an issue coming from interoperability with C server
			// In C version, 'crc' is a 'long long unsigned'!
			if (crc == -1) crc = 0xffffffffffffffffL;
			if (crc != sensor.crc) {
				tcpOut.warning("Sensor " + String.format("%02d", sensor.sensorId) + ": Wrong CRC (" + hex(crc) + " instead of " + hex(sensor.crc) + ")");
			} else {
				data.add(sensor);
			}
		}
		return data;
	}
	//--------------------------------------------------------------------------
	public static ArrayList<sensorIF.sensorTable> getSensorTable() throws Exception {
		byte[] bacli = sensorIF.packInt(R_TABLES_REQUESTED);
		try {
			outToServer.write(bacli, 0, bacli.length);
			outToServer.flush();
		} catch (Exception e) {
			tcpOut.error("Exception occurred when getting table - " + e.getMessage());
		}
		ArrayList<sensorIF.sensorTable> tables = new ArrayList<>();
		try {
			//noinspection StatementWithEmptyBody
			while (0 == inFromServer.available());
		} catch (Exception e) {
			tcpOut.error("Exception occurred when waiting server - " + e.getMessage());
		}
		while (true) {
			sensorIF.sensorTable table = new sensorIF.sensorTable();
			table.name = new char[12];
			byte[] basrv1 = new byte[28];
			try {
				inFromServer.readFully(basrv1, 0, basrv1.length);
			} catch (Exception e) {
				tcpOut.error("Exception occurred when reading table - " + e.getMessage());
			}
			table.unpack(basrv1);
			if (table.size == 0) break;
			long crc = table.computeCRC();
			if (crc != table.crc) {
				tcpOut.warning("Table: Wrong CRC (" + crc + " instead of " + table.crc + ")");
			} else {
				tables.add(table);
			}
		}
		return tables;
	}
	//--------------------------------------------------------------------------
	private static String hex(long n) {
		// call toUpperCase() if that's required
		return String.format("0x%8s", Long.toHexString(n)).replace(' ', '0');
	}
}
