//------------------------------------------------------------------------------
// Copyright (c) 2017, 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class sensorIF {
	//==========================================================================
	public static byte[] packInt(int value) {
		byte[] ba = new byte[4];
		ByteBuffer bb = ByteBuffer.wrap(ba);
		bb.order(ByteOrder.BIG_ENDIAN); // or LITTLE_ENDIAN
		bb.putInt(value);
		return ba;
	}
	//==========================================================================
	public static class sensorRequest {	// R_Sensor_t - Size: 28 bytes
		public int sensorId;
		public long timestart;
		public long timeend;
		public long crc;
		//----------------------------------------------------------------------
		public byte[] pack() {
			byte[] bytes = new byte[28];
			ByteBuffer buffer = ByteBuffer.wrap(bytes);
			//buffer.order(ByteOrder.LITTLE_ENDIAN); // or BIG_ENDIAN
			buffer.putInt(0, sensorId);
			buffer.putLong(4, timestart);
			buffer.putLong(12, timeend);
			buffer.putLong(20, crc);
			return buffer.array();
		}
		//----------------------------------------------------------------------
		public long computeCRC() {
			return (long) sensorId + timestart + timeend;
		}
	}
	//==========================================================================
	public static class sensorData {	// A_Sensor_t - Size: 24 bytes
		public int sensorId;
		public long time;
		public float value;
		public long crc;
		//----------------------------------------------------------------------
		public void unpack(byte[] bytes) {
			ByteBuffer bb = ByteBuffer.wrap(bytes);
			bb.order(ByteOrder.LITTLE_ENDIAN); // or BIG_ENDIAN
			sensorId = bb.getInt();
			time = bb.getLong();
			value = bb.getFloat();
			crc = bb.getLong();
		}
		//----------------------------------------------------------------------
		public long computeCRC() {
			return (long) sensorId + time + (long) value;
		}
	}
	//==========================================================================
	public static class sensorTable {	// R_TableSent_t - Size: 28 bytes
		public char[] name;
		public int size;
		public int rows;
		public long crc;
		//----------------------------------------------------------------------
		public void unpack(byte[] bytes) {
			ByteBuffer bb = ByteBuffer.wrap(bytes);
			bb.order(ByteOrder.LITTLE_ENDIAN); // or BIG_ENDIAN
			size = bb.getInt(12);
			rows = bb.getInt(16);
			crc = bb.getLong(20);
			for (int i = 0; i < 12; i++) name[i] = 0;
			for (int i = 0; i < size; i++) name[i] = (char) bytes[i];
		}
		//----------------------------------------------------------------------
		public long computeCRC() {
			long crc = 0;
			for (int i = 0; i < 12; i++) crc = crc + (long) name[i];
			return crc + (long) size + rows;
		}
	}
}
