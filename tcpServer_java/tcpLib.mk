#------------------------------------------------------------------------------
# Copyright (c) 2019, Michel RIZZO.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Project: tcpServer
# Multithreaded Socket & Database Server
#-------------------------------------------------------------------------------
PROGRAM			= tcpLib
BINJAR			= ../tcpServer_bin
IDEA_PRJ		= $(PROGRAM)
TCPOUT			= $(notdir $(shell ls $(BINJAR)/tcpOut*))
JARS			= $(TCPOUT)
VERSINFO		= $(IDEA_PRJ)/src/tcpLibBuildInfo.java
MOD_VERSINFO	= $(IDEA_PRJ)/src/modified_tcpLibBuildInfo.java
SOURCES			= $(IDEA_PRJ)/src/tcpLib.java \
				  $(IDEA_PRJ)/src/sensorUplet.java \
				  $(IDEA_PRJ)/src/sensorIF.java

include ../.make/java.mk
#-------------------------------------------------------------------------------

