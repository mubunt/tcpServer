## APPLICATION: *tcpServer* and Co.

**tcpServer**. and Co. are It a set of applications whose core is a multithreaded server allowing to record in a database (SQLITE for this work) data sent over TCP / IP by several clients and to restore them on demand  to several clients.. The operating diagram can be illustrated as follows.

![tcpServer](README_images/tcpServer-00.png  "tcpServer")

The APIs are minimal because just the feasibility (and the verification of our poor knowledge) interested us.

Generated data  sent to server is made up:
- a  time: 32-bit long for  the Unix time of the measure .
- a value: the float  value of the measure
- the cyclic redundancy check value -unsigned integer) associated to the global data.

Besides the client applications supposed to simulate the acquisition and the restoration of the data, we have added an optional  "spy" application ("sensorCounters")  which makes it possible to check that the system of acquisition and recording is in operation.

## LICENSE

**tcpServer** is covered by the GNU General Public License (GPL) version 3 and above.

## Example OF USAGE  tcpServer & co
**Example 1:**
![tcpServer](README_images/tcpServer-01.png  "tcpServer")

**Example 2:** Linux with SWT Reporter
![tcpServer](README_images/tcpServer-03.png  "tcpServer")

## STRUCTURE OF THE APPLICATION
This section walks you through **tcpServer**'s structure. Once you understand this structure, you will easily find your way around in **tcpServer**'s code base.
``` Bash
$ yaTree
./                                        # Application level
├── README_images/                        # Images for documentation
│   ├── tcpServer-00.png                  # 
│   ├── tcpServer-01.png                  # 
│   └── tcpServer-03.png                  # 
├── tcpServer_bin/                        # Binary directory: jars (third-parties and local) and driver
│   ├── Makefile                          # -- Makefile
│   ├── commons-cli-1.4.jar               # -- Third-party COMMONS CLI jar file
│   ├── sqlite-jdbc-3.27.2.1.jar          # -- Third-party SQLITE JDBC jar file
│   ├── swt-linux-412.jar                 # -- Third-party SWT jar file
│   └── tcpServerAll                      # -- Script for launching application
├── tcpServer_c/                          # C Source directory
│   ├── Makefile                          # -- Makefile
│   ├── tcpServer.c                       # -- C main source file (application driver)
│   └── tcpServer.ggo                     # -- 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── tcpServer_java/                       # JAVA Source directory
│   ├── clientAutomaton/                  # -- IntelliJ clientAutomaton project structure
│   │   ├── src/                          # 
│   │   │   ├── clientAutomaton.java      # 
│   │   │   └── clientBuildInfo.java      # 
│   │   └── clientAutomaton.iml           # 
│   ├── clientAutomaton2/                 # -- IntelliJ clientAutomaton2 project structure
│   │   ├── src/                          # 
│   │   │   ├── clientAutomaton2.java     # 
│   │   │   └── clientBuildInfo.java      # 
│   │   └── clientAutomaton2.iml          # 
│   ├── clientReporter/                   # -- IntelliJ clientReporter project structure
│   │   ├── src/                          # 
│   │   │   ├── clientBuildInfo.java      # 
│   │   │   └── clientReporter.java       # 
│   │   └── clientReporter.iml            # 
│   ├── clientReporter2/                  # -- IntelliJ clientReporter2 project structure
│   │   ├── src/                          # 
│   │   │   ├── clientBuildInfo.java      # 
│   │   │   └── clientReporter2.java      # 
│   │   └── clientReporter2.iml           # 
│   ├── tcpLib/                           # -- IntelliJ tcpLib project structure
│   │   ├── src/                          # 
│   │   │   ├── sensorIF.java             # 
│   │   │   ├── sensorUplet.java          # 
│   │   │   ├── tcpLib.java               # 
│   │   │   └── tcpLibBuildInfo.java      # 
│   │   └── tcpLib.iml                    # 
│   ├── tcpOut/                           # -- IntelliJ tcpOut project structure
│   │   ├── src/                          # 
│   │   │   ├── tcpOut.java               # 
│   │   │   └── tcpOutBuildInfo.java      # 
│   │   └── tcpOut.iml                    # 
│   ├── tcpServer/                        # -- IntelliJ tcpServer project structure
│   │   ├── src/                          # 
│   │   │   ├── dataBase.java             # 
│   │   │   ├── sensorRequest.java        # 
│   │   │   ├── sensorTable.java          # 
│   │   │   ├── sensorUplet.java          # 
│   │   │   ├── tcpServer.java            # 
│   │   │   ├── tcpServerAcquisition.java # 
│   │   │   ├── tcpServerBuildInfo.java   # 
│   │   │   └── tcpServerReporting.java   # 
│   │   └── tcpServer.iml                 # 
│   ├── Makefile                          # -- Main Makefile
│   ├── clientAutomaton.mk                # -- clientAutomaton Makefile
│   ├── clientAutomaton2.mk               # -- clientAutomaton2 Makefile
│   ├── clientReporter.mk                 # -- clientReporter Makefile
│   ├── clientReporter2.mk                # -- clientReporter2 Makefile
│   ├── tcpLib.mk                         # -- tcpLib Makefile
│   ├── tcpOut.mk                         # -- tcpOut Makefile
│   └── tcpServer.mk                      # -- tcpServer Makefile
├── COPYING.md                            # GNU General Public License markdown file
├── LICENSE.md                            # License markdown file
├── Makefile                              # Makefile
├── README.md                             # ReadMe markdown file
├── RELEASENOTES.md                       # Release Notes markdown file
└── VERSION                               # Version identification text file

18 directories, 54 files
$
```

## HOW TO BUILD THESE APPLICATIONS
``` Bash
	$ cd tcpServer
	$ make clean all
```

## HOW TO INSTALL THIS APPLICATION
``` Bash
	$ cd tcpServer
	$ make release
		# Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    	# We consider that $BIN_DIR is a part of the PATH.
```

## HOW USE THIS APPLICATION
Way #1:
``` Bash
	$ tcpServer [--tcpServer] [--verbose]  [--spy] [--server=....]
	$ tcpServer --sensor --server=....    # only if "--spy" option used
	$ tcpServer --acquisition --server=.... [-verbose]
	$ tcpServer --report --server=.... [-verbose]
```

Way #2:
``` Bash
	$ tcpServerAll	# linux
```

## HOW TO PLAY WITH JAVA SOURCES
To play with, we recommend to use **IntelliJ IDEA**, on a Linux platform:
- Launch IntelliJ IDEA
- Click on **File -> Open...** and select *tcpServer/tcpServer_java/tcpServer or clientAutomaton or clientReporter or sensorCounters project
- It's your turn...

## SOFTWARE REQUIREMENTS
- For usage:
	- JAVA 1.8.0 for usage and development
- For development:
  - IntelliJ IDEA 2019.2.4 (Community Edition) Build #IC-192.7142.36, built on October 29, 2019
  - Openjdk:
    - version "1.8.0_232-ea"
    - OpenJDK Runtime Environment (build 1.8.0_232-ea-8u232-b09-0ubuntu1-b09)
    - OpenJDK 64-Bit Server VM (build 25.232-b09, mixed mode)
  - GNU gengetopt 2.22.6 

Application developed and tested with UBUNTU 16.10 / UBUNTU 17.04 / XUBUNTU 17.04.

## RELEASE NOTES
Refer to file [RELEASENOTES](RELEASENOTES.md) .

***