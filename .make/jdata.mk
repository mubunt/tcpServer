#------------------------------------------------------------------------------
# Copyright (c) 2020, Michel RIZZO.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# CONTEXT
# -------
#	Data file processing for C/Java projects
#
# INPUTS
# ------
# 	ENVIRONMENT VARIABLES:
#		BIN_DIR			Pathname of released executables
#		LIB_DIR			Pathname of released libraries
#		INC_DIR			Pathname of released header files
#
# 	MAKE VARIABLES:
#		MUTE			Prevents or not the command line from echoing out to the console
#		BINJAR 			Relative path to directory where are stored jar files
#		JARS 			List of jar files
#		JARLIBS			List of jar dependencies
#		IMAGES 			List of image files to install
#		IMAGEDIR 		Name of image installation directory, relative to $(BINJAR)
#		SOUNDS 			List of sound files to install
#		SOUNDDIR 		Name of sound installation directory, relative to $(BINJAR)
#		SCRIPTS 		List of script files to install in $(BINJAR)
#		DATA 			List of other data files to install in $(BINJAR)
#------------------------------------------------------------------------------
IMAGEDIR 	= images
SOUNDDIR	= sounds
MKDIR		= mkdir -p
INSTALL		= install -p -v -D
RM			= rm -f
RMDIR		= rm -fr
#-------------------------------------------------------------------------------
clean:
	@echo "-- Removing generated jar files"
	$(MUTE)$(RM) $(JARS)
install:
	$(MUTE)$(MKDIR) $(BIN_DIR)/$(BINJAR)
	@echo "-- Copying JAR files to $(BIN_DIR)/$(BINJAR)"
	$(MUTE)$(INSTALL) -m 644 $(JARS) $(JARLIBS) $(BIN_DIR)/$(BINJAR)
	$(MUTE)if [ ! -z "$(IMAGES)" ]; then \
		$(MKDIR) $(BIN_DIR)/$(BINJAR)/$(IMAGEDIR); \
		echo "-- Copying IMAGES files to $(BIN_DIR)/$(BINJAR)/$(IMAGEDIR)"; \
		$(INSTALL) -m 644 $(IMAGES) $(BIN_DIR)/$(BINJAR)/$(IMAGEDIR); \
	fi
	$(MUTE)if [ ! -z "$(SOUNDS)" ]; then \
		$(MKDIR) $(BIN_DIR)/$(BINJAR)/$(SOUNDDIR); \
		echo "-- Copying SOUNDS files to $(BIN_DIR)/$(BINJAR)/$(SOUNDDIR)"; \
		$(INSTALL) -m 644 $(SOUNDS) $(BIN_DIR)/$(BINJAR)/$(SOUNDDIR); \
	fi
	$(MUTE)if [ ! -z "$(DATA)" ]; then \
		echo "-- Copying DATA files to $(BIN_DIR)/$(BINJAR)"; \
		$(INSTALL) -m 644 $(DATA) $(BIN_DIR)/$(BINJAR)/; \
	fi
	$(MUTE)if [ ! -z "$(SCRIPTS)" ]; then \
		echo "-- Copying SCRIPTS files to $(BIN_DIR)/$(BINJAR)"; \
		$(INSTALL) -m 750 $(SCRIPTS) $(BIN_DIR)/$(BINJAR)/; \
	fi
cleaninstall:
	@echo "-- Removing $(BIN_DIR)/$(BINJAR) directory"
	$(MUTE)$(RMDIR) $(BIN_DIR)/$(BINJAR)
#-------------------------------------------------------------------------------
.PHONY: clean install cleaninstall
#-------------------------------------------------------------------------------