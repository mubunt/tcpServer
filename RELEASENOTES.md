# RELEASE NOTES: *tcpServer*, a Multithreaded Socket & Database Server

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.6.7**:
  - Updated build system components.

- **Version 1.6.6**:
  - Updated build system.

- **Version 1.6.5**:
  - Removed unused files.

- **Version 1.6.4**:
  - Updated build system component(s)

- **Version 1.6.3**:
  - Reworked build system to ease global and inter-project updated.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.6.2**:
  - Updated SWT library from 4.12 to 4.15.

- **Version 1.6.1**:
  - Removed build method for Windows and associated files.

- **Version 1.5.1**:
  - Some minor changes in project structure and build.

- **Version 1.5.0**:
``` bash
tcpServer        Build 33 - November, 2019
tcpOut           Build 1 - November, 2019
tcpLib           Build 3 - November, 2019
clientAutomaton  Build 14 - November, 2019
clientAutomaton2 Build 2 - November, 2019
clientReporter   Build 15 - November, 2019
clientReporte2   Build 5 - November, 2019
```
  - Moved log routines (error, warning, verbose) in a specific common library.
  - Added new acquisition client application simulating 16 clients / sensors.
  - Move source code of *sensorCounters* application in new acquisition client application.
  - Reworked the set for robustification.

- **Version 1.4.1**:
``` bash
tcpServer        Build 31 - November, 2019
tcpLib           Build 2 - November, 2019
clientAutomaton  Build 13 - November, 2019
clientReporter   Build 14 - November, 2019
clientReporte2   Build 4 - November, 2019
sensorCounters   Build 10 - November, 2019
```
  - Simplified the launching process given as an example for Linux
  - Slightly improved the graphical client reporting.
  - Improved server error reporting (catching and readability).

- **Version 1.4.0**:
``` bash
tcpServer        Build 31 - November, 2019
tcpLib           Build 1 - November, 2019
clientAutomaton  Build 12 - November, 2019
clientReporter   Build 13 - November, 2019
clientReporte2   Build 3 - November, 2019
sensorCounters   Build 9 - November, 2019
```
  - Moved communication routines with server and associated data in a specific common library.

- **Version 1.3.0**:
``` bash
tcpServer        Build 31 - November, 2019
clientAutomaton  Build 11 - November, 2019
clientReporter   Build 12 - November, 2019
clientReporte2   Build 2 - November, 2019
sensorCounters   Build 8 - November, 2019
```
  - Added new reporting client with graphical output (clientReporte2, SWT based).
  - Added SWT 4.12.
  - Added new screenshot.

- **Version 1.2.0**:
``` bash
tcpServer        Build 31 - November, 2019
clientAutomaton  Build 11 - November, 2019
clientReporter   Build 12 - November, 2019
sensorCounters   Build 7 - November, 2019
```
  - Replaced license files (COPYNG and LICENSE) by markdown version.
  - Replaced Release Nores file (this file) by markdown version.
  - Removed *index.html* file (was used for *GitHub*).
  - Moved from GPL v2 to GPL v3.
  - Moved to SQLITE JDBC 3.27.2.1. Initially was 3.8.11.2.
  - Improved ./Makefile, ./tcpServer_bin/Makefile, ./tcpServer_c/Makefile, ./tcpServer_java/Makefile.
  - Removed C compilation warnings.
  - Updated README file.

- **Version 1.1.1**:
``` bash
tcpServer        Build 30 - January, 2017
clientAutomaton  Build 10 - January, 2017
clientReporter   Build 11 - May, 2017
sensorCounters   Build 6 - May, 2017
```
  - Fixed typos and omission in README file.

- **Version 1.1.0**:
 ``` bash
tcpServer        Build 30 - January, 2017
clientAutomaton  Build 10 - January, 2017
clientReporter   Build 11 - May, 2017
sensorCounters   Build 6 - May, 2017
```
  - Drivers are now a C executable. Were script (bash / cmd) files.
  - Introduced cross generation for Windows (make wall|wclean|winstall|wcleaninstall).
  - Move to Commons CLI 1.4. Previously was 1.3.1

- **Version 1.0.0**:
``` bash
tcpServer        Build 30 - January, 2017
clientAutomaton  Build 10 - January, 2017
clientReporter   Build 10 - January, 2017
sensorCounters   Build 5 - January, 2017
```
  - Initial revision
